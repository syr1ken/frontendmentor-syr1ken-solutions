var menuController = (function () {
  var state = false;

  return {
    getState: function () {
      return state;
    },

    setState: function (value) {
      state = value;
    }
  };
})();

var UIController = (function () {
  var DOMStrings = {
    menuButton: 'ham',
    menu: 'nav-ul',
    navActive: 'nav_active',
    hamIcon: 'ham_icon',
    fontawesome_bars: 'fa-bars',
    fontawesome_close: 'fa-times',
    active: 'active'
  };

  return {
    getDOMStrings: function () {
      return DOMStrings;
    },

    getMenuElement: function () {
      return document.getElementById(DOMStrings.menu);
    },

    getHamIcon: function () {
      return document.getElementById(DOMStrings.hamIcon);
    },

    showMenu: function (menu, hamIcon) {
      menu.classList.add(DOMStrings.navActive);
      console.log(hamIcon)
      hamIcon.classList.remove(DOMStrings.fontawesome_bars);
      hamIcon.classList.add(DOMStrings.fontawesome_close);
    },

    closeMenu: function (menu, hamIcon) {
      menu.classList.remove(DOMStrings.navActive);
      hamIcon.classList.remove(DOMStrings.fontawesome_close);
      hamIcon.classList.add(DOMStrings.fontawesome_bars);
    }
  };
})();

var controller = (function (MenuCtrl, UICtrl) {
  var DOM  = UICtrl.getDOMStrings();


  var setUpEventListeners = function () {
    document.getElementById(DOM.menuButton).addEventListener('click', toggleMenu);
  };

  var toggleMenu = function () {
    event.preventDefault();

    var menu = UICtrl.getMenuElement();
    var hamIcon = UICtrl.getHamIcon();

    if(!MenuCtrl.getState()) {
      UICtrl.showMenu(menu, hamIcon);
      MenuCtrl.setState(true);
      return;
    }

    UICtrl.closeMenu(menu, hamIcon);
    MenuCtrl.setState(false);
  };

  return {
    init: function () {
      setUpEventListeners();
    }
  };
})(menuController, UIController);

controller.init();
