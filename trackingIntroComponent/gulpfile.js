var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var pug = require('gulp-pug');
var sass = require('gulp-sass');
var imagemin = require('gulp-imagemin');
var useref = require('gulp-useref');
var gulpif = require('gulp-if');
var minifyCss = require('gulp-clean-css');
var uglifyJs = require('gulp-uglify');
var autoprefixer = require('gulp-autoprefixer');

// Move fonts to dist
gulp.task('fonts', function () {
  return gulp.src('app/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'));
});

// Preprocces sass files on src and autoprefix
gulp.task('sass', function (done) {
  return gulp.src('app/css/sass/style.sass')
    .pipe(sass())
    .pipe(gulp.dest('app/css'))
    .pipe(autoprefixer())
    .pipe(browserSync.stream());
  done();
});

// Process Pug
gulp.task('pug', function buildHTML() {
  return gulp.src('app/**/*.pug')
  .pipe(pug())
  .pipe(gulp.dest('app'))
  .pipe(browserSync.stream());
});

// Move optimized .html to dist and minify css
gulp.task('html', function () {
  return gulp.src('app/*.html')
    .pipe(useref())
    .pipe(gulpif('*.css', minifyCss()))
    .pipe(gulp.dest('dist'));
});

// Minify Css
gulp.task('minify-css', function (done) {
  return gulp.src('app/**/*.css')
    .pipe(minifyCss())
    .pipe(gulp.dest('dist'));
});

// Uglify JS
gulp.task('uglify-js', function (done) {
  return gulp.src('app/**/*.js')
    .pipe(uglifyJs())
    .pipe(gulp.dest('dist'));
});

// Minify images
gulp.task('images', function () {
  return gulp.src('app/img/**/*.+(png|jpg|gif|svg)')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/img'));
});

// Live reload server
gulp.task('browser-sync', function(done) {
  browserSync.init({
    server: {
      baseDir: 'app'
    },
    browser: 'chromium',
    notify: false
  });

  browserSync.watch('app').on('change', browserSync.reload);
  done();
});

// Watching and live reloading GULP v4
gulp.task('watch', gulp.series('sass', 'pug', 'browser-sync', function (done) {
  gulp.watch('app/css/**/*.sass', gulp.series('sass'));
  gulp.watch('app/**/*.pug', gulp.series('pug'));
  done();
}));

// Create directories structure for project
gulp.task('directories', function (done) {
  return gulp.src('*.*', {read: false})
    .pipe(gulp.dest('./app'))
    .pipe(gulp.dest('./app/img'))
    .pipe(gulp.dest('./app/fonts'))
    .pipe(gulp.dest('./app/css'))
    .pipe(gulp.dest('./app/css/sass'))
    .pipe(gulp.dest('./app/js'))
    .pipe(gulp.dest('./dist'));
  done();
});

// Dist
gulp.task('dist', gulp.series('fonts', 'html', 'images', 'minify-css', 'uglify-js', function (done) {
  done();
}))

// Default task
gulp.task('default', gulp.series('sass', 'pug', 'dist', function (done) {
  done();
}));
