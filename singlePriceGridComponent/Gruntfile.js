module.exports = function(grunt) {
  // Module dependencies
  var path = require("path").join;
  var browserSync = require('browser-sync').create();

  var root = path(__dirname);

  // Configuration
  grunt.initConfig({
    watch: {
      cssproc: {
        files: 'app/css/stylus/*.styl',
        tasks: ['stylus', 'autoprefixer']
      },
      pugproc: {
        files: 'app/*.pug',
        tasks: ['pug']
      }
    },
    cssmin: {
      target: {
        files: [{expand: true, cwd: 'app/css', src: ['*.css', '!*.min.css'], dest: 'dist/css', ext: '.css'}]
      }
    },
    stylus: {
      compile: {
        files: [{expand: true, cwd: 'app/css/stylus', src: ['style.styl'], dest: 'app/css/', ext: '.css'}]
      },
    },
    autoprefixer: {
      options: {
        browsers: ['last 4 versions', 'ie 8', 'ie 9']
      },
      my_target: {
        files: [{expand: true, cwd: 'app/css', src: ['*.css', '!*.min.css'], dest: 'app/css', ext: '.css'}]
      }
    },
    pug: {
      compile: {
        files: [{expand: true, cwd: 'app', src: ['*.pug'], dest: 'app/', ext: '.html'}]
      }
    },
    imagemin: {
      dynamic: {
        files: [{expand: true, cwd: 'app/img', src: ['**/*.{png,jpg,gif,svg}'], dest: 'dist/img'}]
      }
    },
    copy: {
      fonts: {
        files: [{expand: true, cwd: 'app/fonts', src: ['**/*.{eot,ttf,woff,woff2}'], dest: 'dist/fonts'}]
      },
      html: {
        files: [{expand: true, cwd: 'app', src: ['*.html'], dest: 'dist'}]
      }
    }
  });

  // Load plugins
  // Watch
  grunt.loadNpmTasks('grunt-contrib-watch');
  // Minify images
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  // Minify css
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  // Autoprefixer
  grunt.loadNpmTasks('grunt-autoprefixer');
  // Stylus compiler
  grunt.loadNpmTasks('grunt-contrib-stylus');
  // Pug compiler
  grunt.loadNpmTasks('grunt-contrib-pug');
  // Copy files
  grunt.loadNpmTasks('grunt-contrib-copy');

  //Register tasks
  grunt.registerTask('directories', function () {
    grunt.file.mkdir(root + '/app');
    grunt.file.mkdir(root + '/dist');
    grunt.file.mkdir(root + '/app/css');
    grunt.file.mkdir(root + '/app/css/stylus');
    grunt.file.mkdir(root + '/app/img');
    grunt.file.mkdir(root + '/app/fonts');
  });

  // Live reload server
  grunt.registerTask('browser-sync', function () {
    browserSync.init({
      server: {
        baseDir: 'app'
      },
      browser: 'chromium',
      notify: false
    });

    browserSync.watch('app').on('change', browserSync.reload);
  });

  // Server
  grunt.registerTask('server', ['browser-sync', 'watch']);

  // Move everything to dist
  grunt.registerTask('dist', ['cssmin', 'imagemin', 'copy']);

  // Preset
  grunt.registerTask('preser', ['stylus', 'autoprefixer', 'pug']);

  // Default task
  grunt.registerTask('default', ['stylus', 'autoprefixer', 'pug', 'dist']);

};
