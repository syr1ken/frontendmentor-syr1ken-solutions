# Frontend Mentor Solutions

This is my repository where I upload my solutions of [frontend mentor](https://www.frontendmentor.io/challenges) challenges.

Frontend mentor is a web platform with a community over 33,000 developers that build projects, review code and help each other get better. This platform has collection of web designs which you can pick and build from scratch and then upload your project and get feedback on your code from other developers in the community.

Below you can see my solutions of some challenges that I picked. I sort all of my solutions by difficulty, on the top of this document you can see newbie difficulty challenges and in the end the most difficult challenges.

Each project section below has links which you can visit to see the challenge on frontend mentor, discription or demo of my solution for the challenge.

---

#### Four Card Features Section

For this challenge I used: gulp, sass, pug  
[Challenge](https://www.frontendmentor.io/challenges/four-card-feature-section-weK1eFYK)  
[Solution Demo](https://syr1ken.gitlab.io/frontendmentor-syr1ken-solutions/fourCardFeaturesSection/dist/index.html)

#### Huddle Single Introductory Section

For this challenge I used: gulp, sass, pug  
[Challenge](https://www.frontendmentor.io/challenges/huddle-landing-page-with-a-single-introductory-section-B_2Wvxgi0)  
[Solution Demo](https://syr1ken.gitlab.io/frontendmentor-syr1ken-solutions/huddleSingleIntroductorySection/dist/index.html)

### Single Price Grid Component

For this challenge I used: grunt, stylus, pug  
[Challenge](https://www.frontendmentor.io/challenges/single-price-grid-component-5ce41129d0ff452fec5abbbc)  
[Solution Demo](https://syr1ken.gitlab.io/frontendmentor-syr1ken-solutions/singlePriceGridComponent/dist/index.html)

### Ping Coming Soon Page Master

For this challenge I used: grunt, stylus, pug, ES5  
[Challenge](https://www.frontendmentor.io/challenges/ping-single-column-coming-soon-page-5cadd051fec04111f7b848da)  
[Solution Demo](https://syr1ken.gitlab.io/frontendmentor-syr1ken-solutions/pingComingSoonPageMaster/dist/index.html)

### Base Apparel Coming Soon Master

For this challenge I used: gulp, less, pug, ES5  
[Challenge](https://www.frontendmentor.io/challenges/base-apparel-coming-soon-page-5d46b47f8db8a7063f9331a0)  
[Solution Demo](https://syr1ken.gitlab.io/frontendmentor-syr1ken-solutions/baseApparelComingSoonMaster/dist/index.html)

### Tracking Intro Component

For this challenge I used: gulp, sass, pug, ES5  
[Challenge](https://www.frontendmentor.io/challenges/project-tracking-intro-component-5d289097500fcb331a67d80e)  
[Solution Demo](https://syr1ken.gitlab.io/frontendmentor-syr1ken-solutions/trackingIntroComponent/dist/index.html)

### Intro Component With Signup Form

For this challenge I used: gulp, less, pug, ES5  
[Challenge](https://www.frontendmentor.io/challenges/intro-component-with-signup-form-5cf91bd49edda32581d28fd1)  
[Solution Demo](https://syr1ken.gitlab.io/frontendmentor-syr1ken-solutions/introComponentWithSignupForm/dist/index.html)

### Article Preview Component

For this challenge I used: grunt, sass, pug, ES5  
[Challenge](https://www.frontendmentor.io/challenges/article-preview-component-dYBN_pYFT)  
[Solution Demo](https://syr1ken.gitlab.io/frontendmentor-syr1ken-solutions/articlePreviewComponent/dist/index.html)

### Fylo Data Storage Component

For this challenge I used: grunt, sass, pug, ES5  
[Challenge](https://www.frontendmentor.io/challenges/fylo-data-storage-component-1dZPRbV5n)  
[Solution Demo](https://syr1ken.gitlab.io/frontendmentor-syr1ken-solutions/fyloDataStorageComponent/dist/index.html)

### Huddle Landing Page With Alternating Feature Blocks

For this challenge I used: grunt, less, pug  
[Challenge](https://www.frontendmentor.io/challenges/huddle-landing-page-with-alternating-feature-blocks-5ca5f5981e82137ec91a5100)  
[Solution Demo](https://syr1ken.gitlab.io/frontendmentor-syr1ken-solutions/huddleLandingPageWithAlternatingFeatureBlocks/dist/index.html)

### Fylo Landing Page With Two Column Layout

For this challenge I used: grunt, less, pug  
[Challenge](https://www.frontendmentor.io/challenges/fylo-landing-page-with-two-column-layout-5ca5ef041e82137ec91a50f5)  
[Solution Demo](https://syr1ken.gitlab.io/frontendmentor-syr1ken-solutions/fyloLandingPageWithTwoColumnLayout/dist/index.html)

### Huddle Landing Page With Curved Sections

For this challenge I used: gulp, stylus, pug  
[Challenge](https://www.frontendmentor.io/challenges/huddle-landing-page-with-curved-sections-5ca5ecd01e82137ec91a50f2)  
[Solution Demo](https://syr1ken.gitlab.io/frontendmentor-syr1ken-solutions/huddleLandingPageWithCurvedSections/dist/index.html)

### Fylo Dark Theme Landing Page

For this challenge I used: gulp, stylus, pug  
[Challenge](https://www.frontendmentor.io/challenges/fylo-dark-theme-landing-page-5ca5f2d21e82137ec91a50fd)  
[Solution Demo](https://syr1ken.gitlab.io/frontendmentor-syr1ken-solutions/fyloDarkThemeLandingPage/dist/index.html)

### Clipboard Landing Page

For this challenge I used: gulp, sass, pug  
[Challenge](https://www.frontendmentor.io/challenges/clipboard-landing-page-5cc9bccd6c4c91111378ecb9)  
[Solution Demo](https://syr1ken.gitlab.io/frontendmentor-syr1ken-solutions/clipboardLandingPage/dist/index.html)

### Meet Landing Page

For this challenge I used: gulp, webpack, CSS3, pug  
[Challenge](https://www.frontendmentor.io/challenges/meet-landing-page-rbTDS6OUR)  
[Solution Demo](https://syr1ken.gitlab.io/frontendmentor-syr1ken-solutions/meetLandingPage/dist/index.html)

### Typemaster Pre-launch Landing Page

For this challenge I used: gulp, webpack, CSS3, pug  
[Challenge](https://www.frontendmentor.io/challenges/typemaster-prelaunch-landing-page-J6-Yj5J-X)  
[Solution Demo](https://syr1ken.gitlab.io/frontendmentor-syr1ken-solutions/typeMasterPreLaunchLandingPage/dist/index.html)

### Launch Countdown Timer

For this challenge I used: Vue3, Vue-CLI, Options API, sass, pug, firebase hosting  
[Challenge](https://www.frontendmentor.io/challenges/launch-countdown-timer-N0XkGfyz-)  
[Solution Demo](https://vue-launch-countdown.web.app/)

### Job Listings With Filtering

For this challenge I used: Vue3, Vue-CLI, Options API, Vue-router, Vuex, sass, pug, firebase hosting  
[Challenge](https://www.frontendmentor.io/challenges/job-listings-with-filtering-ivstIPCt)  
[Solution Demo](https://vue-job-listings-58349.web.app/jobs)
