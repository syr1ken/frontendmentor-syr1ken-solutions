var inputController = (function () {
  var INPUTstats, isEmpty, isValidEmail;

  INPUTtypes = {
    email: 'email'
  };

  INPUTstats = {
    empty: 'empty',
    incorrectEmail: 'email',
    success: 'success'
  };

  isEmpty = function (value) {
    if (value === '') return true;
    return false;
  };

  isValidEmail = function (email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  return {
    getValidationStatus: function (type, value) {
      if (isEmpty(value)) return INPUTstats.empty;
      if (type === INPUTtypes.email) {
        if (!isValidEmail(value)) return INPUTstats.incorrectEmail;
      }
      return INPUTstats.success;
    },
    getErrorMessage: function (status, name) {

      var errorMessages = {};

      errorMessages[INPUTstats.empty] = name.charAt(0).toUpperCase() + name.slice(1) + ' cannot be empty';
      errorMessages[INPUTstats.incorrectEmail] = 'Looks like this is not an email';

      return errorMessages[status];
    },
    getInputStats: function () {
      return INPUTstats;
    }
  };
})();

var UIController = (function () {
  var DOMstrings = {
    FORM: 'form',
    INPUT: 'input',
    typeAttr: 'type',
    nameAttr: 'name',
    typeSubmit: '[type="submit"]',
    btnSubmit: '.btn__sumbit',
    errorElem: '.form-error',
    errorImg: '.form-errorimg',
    active: 'active',
    error: 'error'
  };

  return {
    getForm: function (event) {
      var formElement;
      event.path.forEach(function (element) {
         if (element.tagName === DOMstrings.FORM.toUpperCase()) formElement = element;
      });

      return formElement;
    },

    getInputs: function (form) {
      return Array.prototype.slice.call(form.querySelectorAll(DOMstrings.INPUT));
    },

    getValidationErrorElement: function (form, input) {
      return form.querySelector(DOMstrings.errorElem + '[for="' + input.getAttribute(DOMstrings.nameAttr) + '"]');
    },

    getValidationErrorImg: function (form, input) {
      return form.querySelector(DOMstrings.errorImg + '[for="' + input.getAttribute(DOMstrings.nameAttr) + '"]');
    },

    addValidationErrorMessage: function (element, message) {
      element.innerText = message;
    },

    toggleActive: function (element) {
      element.classList.toggle(DOMstrings.active);
    },

    toggleErrorClass: function (element) {
      element.classList.toggle(DOMstrings.error);
    },

    getDOMstrings: function () {
      return DOMstrings;
    }
  };
})();

var controller = (function (inputCtrl, UICtrl) {
  var DOM = UICtrl.getDOMstrings();
  var INPUTstats = inputCtrl.getInputStats();

  var setupEventListeners = function () {
    Array.prototype.slice.call(document.querySelectorAll(DOM.btnSubmit + DOM.typeSubmit)).forEach(function (element) {
      element.addEventListener('click', btnSubmit);
    });
  };

  var btnSubmit = function (event) {
    var form, inputs, status, message, errorElement;

    event.preventDefault();

    form = UICtrl.getForm(event);

    inputs = UICtrl.getInputs(form);

    inputs.forEach(function (input) {
      status = inputController.getValidationStatus(input.getAttribute(DOM.typeAttr), input.value);

      errorElement = UICtrl.getValidationErrorElement(form, input);

      errorImg = UICtrl.getValidationErrorImg(form, input);

      if (errorElement.classList.contains(DOM.active)) UICtrl.toggleActive(errorElement);
      if (input.classList.contains(DOM.error)) UICtrl.toggleErrorClass(input);
      if (errorImg.classList.contains(DOM.active)) UICtrl.toggleActive(errorImg);

      if (status !== INPUTstats.success) {
        message = inputController.getErrorMessage(status, input.getAttribute(DOM.nameAttr));

        UICtrl.addValidationErrorMessage(errorElement, message);

        if (!errorElement.classList.contains(DOM.active)) UICtrl.toggleActive(errorElement);
        if (!input.classList.contains(DOM.error)) UICtrl.toggleErrorClass(input);
        if (!errorImg.classList.contains(DOM.active)) UICtrl.toggleActive(errorImg);
      }
    });
  };

  return {
    init: function () {
      setupEventListeners();
    }
  };
})(inputController, UIController);

controller.init();
