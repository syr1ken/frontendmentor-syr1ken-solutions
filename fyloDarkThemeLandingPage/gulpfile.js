const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const stylus = require('gulp-stylus');
const imagemin = require('gulp-imagemin');
const useref = require('gulp-useref');
const gulpif = require('gulp-if');
const minifyCss = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
var pug = require('gulp-pug');

// Move fonts to dist
gulp.task('fonts', function () {
  return gulp.src('app/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'))
});

gulp.task('pug', function buildHTML() {
  return gulp.src('app/**/*.pug')
  .pipe(pug())
  .pipe(gulp.dest('app'))
  .pipe(browserSync.stream());
});

// Preprocces stylus files on src
gulp.task('stylus', function (done) {
  return gulp.src('app/css/stylus/style.styl')
    .pipe(stylus())
    .pipe(gulp.dest('app/css'))
    .pipe(autoprefixer())
    .pipe(browserSync.stream());

  done();
});

// Move optimized .html to dist and minify css
gulp.task('html', function () {
  return gulp.src('app/*.html')
    .pipe(useref())
    .pipe(gulpif('*.css', minifyCss()))
    .pipe(gulp.dest('dist'));
});

// Minify Css
gulp.task('minify-css', function (done) {
  return gulp.src('app/**/*.css')
    .pipe(minifyCss())
    .pipe(gulp.dest('dist'));
});

// Minify images
gulp.task('images', function () {
  return gulp.src('app/img/**/*.+(png|jpg|gif|svg)')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/img'))
});

// Live reload server
gulp.task('browser-sync', function(done) {
  browserSync.init({
    server: {
      baseDir: 'app',
    },
    browser: 'chromium',
    notify: false
  });

  browserSync.watch('app').on('change', browserSync.reload);
  done();
});

// Watching and live reloading GULP v4
gulp.task('watch', gulp.series('stylus', 'pug', 'browser-sync', function (done) {
  gulp.watch('app/css/stylus/**/*.styl', gulp.series('stylus'));
  gulp.watch('app/**/*.pug', gulp.series('pug'));
  done();
}));

// Create directories structure for project
gulp.task('directories', function (done) {
  return gulp.src('*.*', {read: false})
    .pipe(gulp.dest('./app'))
    .pipe(gulp.dest('./app/img'))
    .pipe(gulp.dest('./app/fonts'))
    .pipe(gulp.dest('./app/css'))
    .pipe(gulp.dest('./app/css/stylus'))
    .pipe(gulp.dest('./dist'));
  done();
});

// Dist
gulp.task('dist', gulp.series('fonts', 'html', 'images', 'minify-css', function (done) {
  done();
}))

// Default task
gulp.task('default', gulp.series('stylus', 'pug', 'dist', function (done) {
  done();
}));
