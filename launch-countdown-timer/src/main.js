import { createApp } from "vue";
import App from "./App.vue";

import BaseCard from "./components/UI/BaseCard.vue";
import BaseSocial from "./components/UI/BaseSocial.vue";

const app = createApp(App);

app.component("base-card", BaseCard);
app.component("base-social", BaseSocial);

app.mount("#app");
