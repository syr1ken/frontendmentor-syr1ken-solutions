var ready = function() {
  document.querySelector(".share__btn").addEventListener("click", toggleActive);
}


// Load Docuemnt
if (document.readyState === "complete" || (document.readyState !== "loading" && !document.documentElement.doScroll)) {
  ready();
} else {
  document.addEventListener("DOMContentLoaded", ready);
}

function toggleActive (e) {
  e.preventDefault();
  document.querySelector(".share__modal").classList.toggle('active');
}
