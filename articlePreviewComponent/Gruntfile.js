module.exports = function(grunt) {
  // Module dependencies
  var path = require("path").join;
  var browserSync = require('browser-sync').create();

  var root = path(__dirname);

  // Configuration
  grunt.initConfig({
    watch: {
      cssproc: {
        files: 'app/css/sass/*.sass',
        tasks: ['sass', 'autoprefixer']
      },
      pugproc: {
        files: 'app/*.pug',
        tasks: ['pug']
      }
    },
    cssmin: {
      target: {
        files: [{expand: true, cwd: 'app/css', src: ['*.css', '!*.min.css'], dest: 'dist/css', ext: '.css'}]
      }
    },
    sass: {
      dist: {
        options: {
          style: 'expanded'
        },
        files: [{expand: true, cwd: 'app/css/sass', src: ['style.sass'], dest: 'app/css/', ext: '.css'}]
      }
    },
    autoprefixer: {
      options: {
        browsers: ['last 4 versions', 'ie 8', 'ie 9']
      },
      my_target: {
        files: [{expand: true, cwd: 'app/css', src: ['*.css', '!*.min.css'], dest: 'app/css', ext: '.css'}]
      }
    },
    pug: {
      compile: {
        files: [{expand: true, cwd: 'app', src: ['*.pug'], dest: 'app/', ext: '.html'}]
      }
    },
    imagemin: {
      dynamic: {
        files: [{expand: true, cwd: 'app/img', src: ['**/*.{png,jpg,gif,svg}'], dest: 'dist/img'}]
      }
    },
    uglify: {
      my_target: {
        files: [{expand: true, cwd: 'app/js', src: ['**/*.js'], dest: 'dist/js', ext: '.js'}]
      }
    },
    copy: {
      fonts: {
        files: [{expand: true, cwd: 'app/fonts', src: ['**/*.{eot,ttf,woff,woff2}'], dest: 'dist/fonts'}]
      },
      html: {
        files: [{expand: true, cwd: 'app', src: ['*.html'], dest: 'dist'}]
      }
    }
  });

  // Load plugins
  // Watch
  grunt.loadNpmTasks('grunt-contrib-watch');
  // Uglify JS
  grunt.loadNpmTasks('grunt-contrib-uglify');
  // Minify images
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  // Minify css
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  // Autoprefixer
  grunt.loadNpmTasks('grunt-autoprefixer');
  // SASS compiler
  grunt.loadNpmTasks('grunt-contrib-sass');
  // Pug compiler
  grunt.loadNpmTasks('grunt-contrib-pug');
  // Copy files
  grunt.loadNpmTasks('grunt-contrib-copy');

  //Register tasks
  grunt.registerTask('directories', function () {
    grunt.file.mkdir(root + '/app');
    grunt.file.mkdir(root + '/dist');
    grunt.file.mkdir(root + '/app/css');
    grunt.file.mkdir(root + '/app/css/sass');
    grunt.file.mkdir(root + '/app/img');
    grunt.file.mkdir(root + '/app/fonts');
    grunt.file.mkdir(root + '/app/js');
  });

  // Live reload server
  grunt.registerTask('browser-sync', function () {
    browserSync.init({
      server: {
        baseDir: 'app'
      },
      browser: 'chromium',
      notify: false
    });

    browserSync.watch('app').on('change', browserSync.reload);
  });

  // Server
  grunt.registerTask('server', ['browser-sync', 'watch']);

  // Move everything to dist
  grunt.registerTask('dist', ['cssmin', 'imagemin', 'uglify', 'copy']);

  // Preset
  grunt.registerTask('preser', ['sass', 'autoprefixer', 'pug']);

  // Default task
  grunt.registerTask('default', ['sass', 'autoprefixer', 'pug', 'dist']);

};
