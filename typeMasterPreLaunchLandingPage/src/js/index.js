// Import Custom Styles
import "../css/styles.css";

const images = {
  logo: require("../img/logo.svg"),
  square: require("../img/pattern-square.svg"),
  compatible: require("../img/icon-compatible.svg"),
  bluetooth: require("../img/icon-bluetooth.svg"),
  battery: require("../img/icon-battery.svg"),
  light: require("../img/icon-light.svg")
};
