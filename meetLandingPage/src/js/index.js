// Import Custom Styles
import "../css/styles.css";

const images = {
  logo: require("../img/logo.svg"),
  videocall: require("../img/image-woman-in-videocall.jpg"),
  videochatting: require("../img/image-women-videochatting.jpg"),
  meeting: require("../img/image-men-in-meeting.jpg"),
  texting: require("../img/image-man-texting.jpg")
};
