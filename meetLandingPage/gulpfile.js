const gulp = require("gulp");

// TASK HANDLDERS
const direcroties = function () {
  return gulp
    .src("*.*", { read: false })
    .pipe(gulp.dest("./src"))
    .pipe(gulp.dest("./src/fonts"))
    .pipe(gulp.dest("./src/css"))
    .pipe(gulp.dest("./src/img"))
    .pipe(gulp.dest("./src/js"))
    .pipe(gulp.dest("./dist"));
};

// TASKS
// Create directories structure for project
gulp.task("directories", direcroties);
