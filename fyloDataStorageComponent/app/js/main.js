var ready = function() {
  refreshOnStart(document.querySelector('#slider'));
  document.querySelector('#slider').addEventListener('input', refreshValues);
}


// Load Docuemnt
if (document.readyState === 'complete' || (document.readyState !== 'loading' && !document.documentElement.doScroll)) {
  ready();
} else {
  document.addEventListener('DOMContentLoaded', ready);
}

function refreshValues() {
  document.querySelector('.data-value').innerText = this.value;
  document.querySelector('.data-value__reverse').innerText = this.getAttribute('max') - this.value;
}

function refreshOnStart(elem) {
  document.querySelector('.data-value').innerText = elem.value;
  document.querySelector('.data-value__reverse').innerText = elem.getAttribute('max') - elem.value;
}
