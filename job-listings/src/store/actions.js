export default {
  addFilter(context, payload) {
    const filters = context.getters.filters;

    if (typeof payload === "string" && !filters.includes(payload)) {
      context.commit("addFilter", payload);
    }
  },
  clearFilters(context) {
    context.commit("clearFilters");
  },
  removeFilter(context, payload) {
    context.commit("removeFilter", payload);
  }
};
