export default {
  addFilter(state, payload) {
    state.filters.push(payload);
  },
  clearFilters(state) {
    state.filters.splice(state.filters, state.filters.length);
  },
  removeFilter(state, payload) {
    state.filters = state.filters.filter((filter) => filter !== payload);
  }
};
