export default {
  jobs(state) {
    return state.jobs;
  },
  hasJobs(state) {
    return state.jobs && state.jobs.length > 0;
  },
  filters(state) {
    return state.filters;
  },
  hasFilters(state) {
    return state.filters && state.filters.length > 0;
  }
};
