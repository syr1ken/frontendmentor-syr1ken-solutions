import { createApp } from "vue";
import App from "./App.vue";

import { library } from "@fortawesome/fontawesome-svg-core";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

library.add(faTimes);

import store from "./store/index.js";
import router from "./router";

import BaseContainer from "./components/UI/BaseContainer.vue";
import BaseBadge from "./components/UI/BaseBadge.vue";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

const app = createApp(App);

app.use(router);
app.use(store);

app.component("base-container", BaseContainer);
app.component("base-badge", BaseBadge);
app.component("font-awesome-icon", FontAwesomeIcon);

app.config.productionTip = false;

app.mount("#app");
