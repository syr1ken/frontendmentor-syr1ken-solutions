import { createRouter, createWebHistory } from "vue-router";

import JobsList from "./pages/JobsList.vue";
import NotFound from "./pages/NotFound.vue";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: "/", redirect: "/jobs" },
    { path: "/jobs", component: JobsList },
    { path: "/:notFound(.*)", component: NotFound }
  ]
});

export default router;
